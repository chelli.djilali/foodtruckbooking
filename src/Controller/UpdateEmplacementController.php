<?php

namespace App\Controller;

use DateTime;
use App\Entity\Emplacement;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\EmplacementRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UpdateEmplacementController extends AbstractController
{
    //   #[Route('/update/emplacement', name: 'app_update_emplacement')]
    private $em;

    public function __construct(EntityManagerInterface $em)
    {

        $this->em = $em;
    }

    public function __invoke(Emplacement $data, Request $request, EmplacementRepository $liste_emplacement_semaine, EntityManagerInterface $em)

    {
        $emplacement = $data;
        $this->em = $em;
        //  $this->em->getRepository(Emplacement::class)->findAllEmplacementFoodTruckSemaineEncours();

        $datedujour = new DateTime();
        $datesouhaité = $emplacement->getEmplacementDate();
        // un peu de traitement ici qu'on peux mettre dans un service quelques part...

        if ($emplacement->getEtat() == true) {
            // $emplacement->setVille('toto');

            die("l'emplacement est déjà pris ");
        }
        //$dateDispo = $emplacement->getEmplacementDate();


        if (date_format($datesouhaité, 'Y-m-d') == date_format($datedujour, 'Y-m-d')) {
            die("vous ne pouvez pas reserver pour le jour même");
            //interdiction de la réservation pour le jour meme!
        }

        $datedujour = date_format($datedujour, 'Y-m-d');
        $good_format = strtotime($datedujour);
        $numeroSemaine =  date('W', $good_format);
        $idFoodTruck = $emplacement->getFoodTruck();
        $res =  $this->em->getRepository(Emplacement::class)->findAllEmplacementSemaine($idFoodTruck);

        if (count($res) > 0) {
            die('Vous avez déjà une reservation pour la semaine');
        }


        //   $res = $this->em->getRepository(Emplacement::class)->findAllEmplacementDisponible();
        //test si il a deja reserver pour la meme semaine, 
        // si oui die() sinon il faut perdister...

        //une requette qui permet de compter le nombre count() si > à 0 cela veut dire qu'il à déjà reserver pour la semaine et donc il faut pas persiste....

        $emplacement->setEtat(true);
        $this->em->persist($emplacement);
        $this->em->flush();
        return $emplacement;
        //$request->attributes->get('id'); pour faire eventuellemnt autre chose, 

    }
}
