<?php

namespace App\Controller;



use App\Entity\Emplacement;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use ApiPlatform\Core\OpenApi\Factory\OpenApiFactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class EmplacementLibreController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function __invoke()
    {
        return $this->em->getRepository(Emplacement::class)->findAllEmplacementDisponible();
    }
}
