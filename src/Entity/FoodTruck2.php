<?php

namespace App\Entity;

use App\Repository\FoodTruck2Repository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: FoodTruck2Repository::class)]
class FoodTruck2
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
