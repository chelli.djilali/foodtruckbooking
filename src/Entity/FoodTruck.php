<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\FoodTruckRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: FoodTruckRepository::class)]
/**

 * @ApiResource(
 * collectionOperations = {"get"},
 * itemOperations = {"get"}
 
 * )

 */
class FoodTruck
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    /**
     * @Groups({"emplacement:read"})
     */
    private $name;

    #[ORM\Column(type: 'integer', nullable: true)]
    private $nb_reservation;

    #[ORM\OneToMany(mappedBy: 'FoodTruck', targetEntity: Emplacement::class)]
    private $emplacements;



    public function __construct()
    {
        $this->emplacements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {

        $this->name = $name;

        return $this;
    }

    public function getNbReservation(): ?int
    {
        return $this->nb_reservation;
    }

    public function setNbReservation(?int $nb_reservation): self
    {
        $this->nb_reservation = $nb_reservation;

        return $this;
    }

    /**
     * @return Collection<int, Emplacement>
     */
    public function getEmplacements(): Collection
    {
        return $this->emplacements;
    }

    public function addEmplacement(Emplacement $emplacement): self
    {
        if (!$this->emplacements->contains($emplacement)) {
            $this->emplacements[] = $emplacement;
            $emplacement->setFoodTruck($this);
        }

        return $this;
    }

    public function removeEmplacement(Emplacement $emplacement): self
    {
        if ($this->emplacements->removeElement($emplacement)) {
            // set the owning side to null (unless already changed)
            if ($emplacement->getFoodTruck() === $this) {
                $emplacement->setFoodTruck(null);
            }
        }

        return $this;
    }
}
