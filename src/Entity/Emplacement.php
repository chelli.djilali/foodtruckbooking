<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\EmplacementRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;

#[ORM\Entity(repositoryClass: EmplacementRepository::class)]

#[ApiResource(
    normalizationContext: ['groups' => ['emplacement:read']],
    denormalizationContext: ['groups' => ['emplacement:write']],
    collectionOperations: [
        'emplacement_libre' => [
            'method' => 'GET',
            'path' => '/emplacements',
            'controller' => 'App\Controller\EmplacementLibreController'::class,

        ],

    ],
    itemOperations: [
        'get' => [],
        'update' => [
            'method' => 'PUT',
            'path' => '/emplacements/reserver/{id}',
            'controller' => 'App\Controller\UpdateEmplacementController'::class,

        ],
        'annulerReservation' => [
            'method' => 'PUT',
            'path' => '/emplacements/Annuler/{id}',
            'controller' => 'App\Controller\UpdateEmplacementController'::class,

        ],


    ],
)]
class Emplacement
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]

    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(["emplacement:read"])]

    private $ville;

    #[ORM\Column(type: 'datetime')]
    #[Groups(["emplacement:read"])]

    private $emplacementDate;


    #[ORM\ManyToOne(targetEntity: FoodTruck::class, inversedBy: 'emplacements')]
    #[Groups(["emplacement:read", "emplacement:write"])]

    private $FoodTruck;

    #[ORM\Column(type: 'boolean')]
    #[Groups(["emplacement:read"])]
    private $etat;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getEmplacementDate(): ?\DateTimeInterface
    {
        return $this->emplacementDate;
    }

    public function setEmplacementDate(\DateTimeInterface $emplacementDate): self
    {
        $this->emplacementDate = $emplacementDate;

        return $this;
    }

    public function getFoodTruck(): ?FoodTruck
    {
        return $this->FoodTruck;
    }

    public function setFoodTruck(?FoodTruck $FoodTruck): self
    {
        $this->FoodTruck = $FoodTruck;

        return $this;
    }

    public function getEtat(): ?bool
    {
        return $this->etat;
    }

    public function setEtat(bool $etat): self
    {
        $this->etat = $etat;

        return $this;
    }
}
