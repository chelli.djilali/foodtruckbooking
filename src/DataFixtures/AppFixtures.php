<?php

namespace App\DataFixtures;

use DateTime;
use Faker\Factory;
use App\Entity\FoodTruck;

use App\Entity\Emplacement;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;


class AppFixtures extends Fixture
{



    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = Factory::create('fr_FR');



        for ($i = 0; $i < 20; $i++) {
            $foodTruck = new FoodTruck();
            $foodTruck->setName("foodTruck de " . $faker->firstName())
                ->setNbReservation($faker->randomDigit);
            $manager->persist($foodTruck);
        }




        # génération des emplacement, j'ai choisi de faire comme cela car c'était plus simple pour moi, 
        # et je me suis dit que c'est la societé Hooly qui sait à l'avance les emplacement, 
        // donc en quelque sort, je met a disposition uniquement les dates disponible pour les foodtrucks


        $begin = new DateTime('2020-08-01');
        $end = new DateTime('2023-08-31');
        // $end = $end->modify($i . 'day');
        $nombreJour = $end->diff($begin)->format("%a");
        $jour = $begin->modify(1 . 'day');

        for ($i = 0; $i < $nombreJour; $i++) {


            if ($jour->format("D") == "Fri")
                $nb_emplacement = 6;
            else
                $nb_emplacement = 7;

            for ($j = 0; $j < $nb_emplacement; $j++) {
                $emplacement = new Emplacement();
                $emplacement->setVille($faker->departmentName())
                    ->setEmplacementDate($jour)
                    ->setEtat($faker->boolean());

                $manager->persist($emplacement);
            }


            dump($jour);
            $jour = $begin->modify(1 . 'day');
            $manager->flush();
        }
    }
}
